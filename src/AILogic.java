import java.util.ArrayList;
import java.util.List;

public class AILogic {
    public AILogic() { }

    public Score minimax(int depth, Game lastGame, Game startingGame) {

        if (lastGame.isGameOver()) {
            if ( lastGame.getWinner() == Player.AI ) {
                return new Score(lastGame, 100 - depth);
            } else {
                return new Score(lastGame, -100 + depth);
            }
        }

        if (depth > 6) {
            return new Score(lastGame, -depth);
        }


        List<Game> allPossibleGameOutcomes = generateAllPossibleGames(lastGame);
        List<Score> scores = new ArrayList<>();

        for(Game game : allPossibleGameOutcomes) {
            Score score = minimax(depth + 1, game, startingGame);
            scores.add(score);
        }

        Score result = null;
        if (lastGame.getTurn() == Player.You) {
            result = getMin(scores);
        } else {
            result = getMax(scores);
        }

        if(lastGame != startingGame) {
            result.setGame(lastGame);
        }

        return result;
    }

    private List<Game> generateAllPossibleGames(Game game) {
        List<Game> allPossibleGameOutcomes = new ArrayList<>();

        for(int i = 0; i < 7; i++) {
            if(game.getTable()[0][i] == Dot.Empty) {
                Game possibleGame = new Game(game);
                possibleGame.placeDot(i);
                allPossibleGameOutcomes.add(possibleGame);
            }
        }

        return allPossibleGameOutcomes;
    }

    private Score getMax(List<Score> scores) {
        Score max = scores.get(0);

        for(Score score : scores) {
            if(score.getValue() > max.getValue()) {
                max = score;
            }
        }

        return max;
    }

    private Score getMin(List<Score> scores) {
        Score min = scores.get(0);

        for(Score score : scores) {
            if(score.getValue() < min.getValue()) {
                min = score;
            }
        }

        return min;
    }
}

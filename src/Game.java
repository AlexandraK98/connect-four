public class Game {
    private Dot[][] table = new Dot[6][7];
    private Player turn;

    public Game() {
        for(int i = 0; i < 6; i++) {
            for(int j = 0; j < 7; j++) {
                table[i][j] = Dot.Empty;
            }
        }
        turn = Player.You;
    }

    public Game(Game other) {
        for(int i = 0; i < 6; i++) {
            for(int j = 0; j < 7; j++) {
                table[i][j] = other.table[i][j];
            }
        }
        turn = other.turn;
    }

    public void placeDot(int column) {
        if(column < 0 || column > 6) {
            throw new RuntimeException("Column must be in range of [0, 6] but was: " + column);
        }

        int currentRow = 0;
        while(currentRow < 6 && table[currentRow][column] == Dot.Empty) {
            currentRow++;
        }

        if(currentRow == 0) {
            throw new RuntimeException("Current column is full!");
        }

        table[currentRow - 1][column] = turn == Player.You ? Dot.Red : Dot.Yellow;
        turn = turn == Player.You ? Player.AI : Player.You;
    }

    public boolean isGameOver() {
        if(isTableFull()) {
            return true;
        }

        for(int i = 0; i < 6; i++) {
            for(int j = 0; j < 4; j++) {
                if(table[i][j] == Dot.Empty) {
                    continue;
                }

                if(table[i][j] == table[i][j+1] && table[i][j] == table[i][j+2] && table[i][j] == table[i][j+3]) {
                    return true;
                }
            }
        }

        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 7; j++) {
                if(table[i][j] == Dot.Empty) {
                    continue;
                }

                if(table[i][j] == table[i+1][j] && table[i][j] == table[i+2][j] && table[i][j] == table[i+3][j]) {
                    return true;
                }
            }
        }

        for(int i = 3; i < 6; i++) {
            for(int j = 0; j < 4; j++) {
                if(table[i][j] == Dot.Empty) {
                    continue;
                }

                if(table[i][j] == table[i-1][j+1] && table[i][j] == table[i-2][j+2] && table[i][j] == table[i-3][j+3]) {
                    return true;
                }
            }
        }

        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 4; j++) {
                if(table[i][j] == Dot.Empty) {
                    continue;
                }

                if(table[i][j] == table[i+1][j+1] && table[i][j] == table[i+2][j+2] && table[i][j] == table[i+3][j+3]) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public String toString() {
        String result = "Table:\n";

        for(int i = 0; i < 6; i++) {
            for(int j = 0; j < 7; j++) {
                if(table[i][j] == Dot.Empty) {
                    result += "_ ";
                } else if(table[i][j] == Dot.Red) {
                    result += "R ";
                } else {
                    result += "Y ";
                }
            }
            result += "\n";
        }

        return result;
    }

    public Player getTurn() {
        return turn;
    }

    public Player getWinner() {
        if(isTableFull() || !isGameOver()) {
            return Player.Nobody;
        }

        return turn == Player.You ? Player.AI : Player.You;
    }

    public Dot[][] getTable() {
        return table;
    }

    private boolean isTableFull() {
        for(int i = 0; i < 7; i++) {
            if(table[0][i] == Dot.Empty) {
                return false;
            }
        }

        return true;
    }
}

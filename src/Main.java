import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Game game = new Game();
        AILogic AILogic = new AILogic();
        Scanner scanner = new Scanner(System.in);

        while(!game.isGameOver()) {
            System.out.println(game);

            if(game.getTurn() == Player.You) {
                System.out.print("Column: ");
                int column = scanner.nextInt();
                game.placeDot(column);
            } else {
                Score score = AILogic.minimax(0, game, game);
                game = score.getGame();
            }
        }

        System.out.println(game);
        System.out.println("Winner is: " + game.getWinner());

    }
}

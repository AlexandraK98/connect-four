public class Score {
    private Game game;
    private int value;

    public Score(Game game, int value) {
        this.game = game;
        this.value = value;
    }

    public Game getGame() {
        return game;
    }

    public int getValue() {
        return value;
    }

    public void setGame(Game game) {
        this.game = game;
    }
}
